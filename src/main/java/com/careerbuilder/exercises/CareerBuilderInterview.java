package com.careerbuilder.exercises;

import com.careerbuilder.exercises.exceptions.CareerBuilderException;
import com.careerbuilder.exercises.utils.StringHelper;
import org.apache.log4j.Logger;

import java.util.Map;

/**
 * Created by wyan on 2/16/2017.
 */
public class CareerBuilderInterview {

    private static Logger logger = Logger.getLogger(CareerBuilderInterview.class);

    public static void main(String args[]) {

        String str = "This is Interview Question from \'CareerBuilder\' @Atlanta ";

        // Exec 1: Given a string, write a function or method that takes in that string, and returns the same string in reverse order.
        try {
            System.out.println("EXEC 1: REVERSE STRING");
            System.out.println(StringHelper.reverseString(str));
        } catch (CareerBuilderException e) {
            logger.error("Error occurs when reverse the string" + str +"!! \n" + "Error Type: " + e.getErrorEnum() +"\n" + "Service: " + e.getServiceName());
        }

        // Exec 2: Given a string, return the character count for each distinct character in the string.
        try {
            System.out.println("EXEC 2: CHAR STATISTIC");
            Map<Character, Integer> dict = StringHelper.countChars(str);
            StringBuilder stringBuilder = new StringBuilder();
            for (Map.Entry<Character, Integer> stat : dict.entrySet()) {
                stringBuilder.append(stat.getKey().equals(' ') ? "Space"
                                        : stat.getKey().equals('\n') ? "Newline"
                                        : stat.getKey().equals('\t') ? "Tab"
                                        : stat.getKey().equals('\r') ? "Return"
                                        : stat.getKey().equals('\b') ? "Backspace"
                                        : stat.getKey().equals('\f') ? "Formfeed"
                                        : stat.getKey())
                            .append(": ").append(stat.getValue()).append("  ");
            }
            System.out.println(stringBuilder.toString());
        } catch (CareerBuilderException e) {
            logger.error("Error occurs when calculate char statistic in string" + str + "!! \n" + "Error Type: " + e.getErrorEnum() +"\n" + "Service: " + e.getServiceName());
        }
    }
}
