package com.careerbuilder.exercises.utils;

import com.careerbuilder.exercises.exceptions.CareerBuilderException;
import com.careerbuilder.exercises.exceptions.ErrorEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by wyan on 2/16/2017.
 */
public class StringHelper {

    /**
     * Exec 1:  Given a string, write a function or method that takes in that string, and returns the same string in reverse order.
     * @param str
     * @return reverse of str
     */
    public static String reverseString (String str) throws CareerBuilderException {

        // validate String value cannot be null. If it is, throw exception that should be caught by service layer and print out.
        validateStringNotNull(str);

        char[] strChars = str.toCharArray();
        int i = 0, j = strChars.length - 1;
        while (i < j) {
            char temp = strChars[i];
            strChars[i] = strChars[j];
            strChars[j] = temp;
            i++;
            j--;
        }

        return String.valueOf(strChars);
    }

    /**
     * Exec 2: Given a string, return the character count for each distinct character in the string.
     * @param str
     * @return map with statistic of chars in str
     */
    public static Map<Character, Integer> countChars (String str) throws CareerBuilderException{

        // validate String value cannot be null. If it is, throw exception that should be caught by service layer and print out.
        validateStringNotNull(str);

        Map<Character, Integer> dict = new HashMap<Character, Integer>();
        for (int i = 0; i < str.length(); i++) {
            if (dict.containsKey(str.charAt(i))) {
                dict.put(str.charAt(i), dict.get(str.charAt(i)).intValue()+1);
            } else {
                dict.put(str.charAt(i), 1);
            }
        }
        return dict;
    }

    private static void validateStringNotNull (String str) throws CareerBuilderException{
        if (str == null) {
            throw new CareerBuilderException(ErrorEnum.NullValueString, StringHelper.class.getName());
        }
    }
}
