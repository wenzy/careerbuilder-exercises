package com.careerbuilder.exercises.exceptions;

/**
 * Created by wyan on 2/16/2017.
 */
public class CareerBuilderException extends Exception{

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 8533617633613986771L;

    /**
     * The error enum.
     */
    private Enum<?> errorEnum = null;

    /**
     * The service name.
     */
    private String serviceName = null;

    /**
     * Instantiates a new CareerBuilder exception handler.
     *
     * @param <T>          the generic type
     * @param e            the e
     * @param errorEnum    the error enum
     * @param serviceName the service name
     */
    public <T extends Enum<T>> CareerBuilderException(Exception e, Enum<T> errorEnum,
                                                     String serviceName)
    {
        super(e);
        this.errorEnum = errorEnum;
        this.serviceName = serviceName;
    }

    /**
     * Instantiates a new CareerBuilder exception handler.
     *
     * @param <T>          the generic type
     * @param errorEnum    the error enum
     * @param serviceName the service name
     */
    public <T extends Enum<T>> CareerBuilderException(Enum<T> errorEnum, String serviceName)
    {
        this.errorEnum = errorEnum;
        this.serviceName = serviceName;
    }

    /**
     * Gets the error enum.
     *
     * @return the error enum
     */
    public Enum<?> getErrorEnum()
    {
        return this.errorEnum;
    }

    /**
     * Gets the service name.
     *
     * @return the service name
     */
    public String getServiceName()
    {
        return this.serviceName;
    }


    @Override
    public void printStackTrace()
    {
        if (getCause() != null)
        {
            getCause().printStackTrace();
        }
        else
        {
            super.printStackTrace();
        }
    }
}
