package com.careerbuilder.exercises.utils;

import com.careerbuilder.exercises.exceptions.CareerBuilderException;
import com.careerbuilder.exercises.exceptions.ErrorEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Map;

import static org.junit.Assert.*;


/**
 * Created by wyan on 2/16/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StringHelper.class })
public class StringHelperTest {

    private String evenCharStr;
    private String oddCharStr;
    private String normalStr;
    private String numberStr;
    private String startSpaceStr;
    private String endSpaceStr;
    private String specialCharStr;
    private String escapeCharStr;
    private String nullStr;
    private String emptyStr;
    private String allSpaceStr;

    @Before
    public void setup() {
        evenCharStr = "even";
        oddCharStr = "odd";
        normalStr = "normalStr";
        numberStr = "Str1";
        startSpaceStr = " space Str";
        endSpaceStr = "space Str ";
        specialCharStr = "@abc#__#)";
        escapeCharStr = "abc\\|\ndef";
        nullStr = null;
        emptyStr = "";
        allSpaceStr = " ";
    }

    @Test
    public void testReverseEvenCharStr() throws CareerBuilderException {
        String str = StringHelper.reverseString(evenCharStr);
        assertEquals(str, "neve");
        assertNotEquals(str, "Neve");
        assertNotEquals(str, "eve");
    }

    @Test
    public void testReverseOddCharStr() throws CareerBuilderException {
        String str = StringHelper.reverseString(oddCharStr);
        assertEquals(str, "ddo");
        assertNotEquals(str, "Doo");
        assertNotEquals(str, "dod");
    }

    @Test
    public void testReverseNormalStr() throws CareerBuilderException {
        String str = StringHelper.reverseString(normalStr);
        assertEquals(str, "rtSlamron");
        assertNotEquals(str, "rtslamron");
        assertNotEquals(str, "normalStr");
    }

    @Test
    public void testReverseNumberStr() throws CareerBuilderException {
        String str = StringHelper.reverseString(numberStr);
        assertEquals(str, "1rtS");
        assertNotEquals(str, "1rts");
    }

    @Test
    public void testReverseStartSpaceStr() throws CareerBuilderException {
        String str = StringHelper.reverseString(startSpaceStr);
        assertEquals(str, "rtS ecaps ");
        assertNotEquals(str, " space Str");
    }

    @Test
    public void testReverseEndSpaceStr() throws CareerBuilderException {
        String str = StringHelper.reverseString(startSpaceStr);
        assertEquals(str, "rtS ecaps ");
        assertNotEquals(str, " space Str");
    }

    @Test
    public void testReverseSpecialCharStr() throws CareerBuilderException {
        String str = StringHelper.reverseString(specialCharStr);
        assertEquals(str, ")#__#cba@");
        assertNotEquals(str, "@abc#__#)");
    }

    @Test
    public void testReverseEscapeCharStr() throws CareerBuilderException {
        String str = StringHelper.reverseString(escapeCharStr);
        assertEquals(str, "fed\n|\\cba");
        assertNotEquals(str, "abc\\|\ndef");
    }


    @Test(expected = CareerBuilderException.class)
    public void testReverseNullStr() throws Exception {
        PowerMockito.mockStatic(StringHelper.class);
        PowerMockito.doThrow(new CareerBuilderException(ErrorEnum.NullValueString, StringHelper.class.getName()))
                .when(StringHelper.class, "reverseString", Matchers.eq(nullStr));
        String str = StringHelper.reverseString(nullStr);
    }

    @Test
    public void testReverseEmptyStr() throws CareerBuilderException {
        String str = StringHelper.reverseString(emptyStr);
        assertEquals(str, "");
    }

    @Test
    public void testReverseAllSpaceStr() throws CareerBuilderException {
        String str = StringHelper.reverseString(allSpaceStr);
        assertEquals(str, " ");
    }

    @Test
    public void testCountCharsNormalStr() throws CareerBuilderException {
        Map<Character, Integer> dict = StringHelper.countChars(normalStr);
        assertTrue(dict.containsKey('n'));
        assertEquals(dict.get('n'), new Integer(1));
        assertEquals(dict.get('r'), new Integer(2));
        assertEquals(dict.get('S'), new Integer(1));
        assertFalse(dict.containsKey('s'));
    }

    @Test
    public void testCountCharsNnumberStr() throws CareerBuilderException {
        Map<Character, Integer> dict = StringHelper.countChars(numberStr);
        assertTrue(dict.containsKey('r'));
        assertEquals(dict.get('S'), new Integer(1));
        assertEquals(dict.get('1'), new Integer(1));
        assertFalse(dict.containsKey('s'));
    }

    @Test
    public void testCountCharsSpaceStr() throws CareerBuilderException {
        Map<Character, Integer> dict = StringHelper.countChars(startSpaceStr);
        assertTrue(dict.containsKey(' '));
        assertEquals(dict.get('S'), new Integer(1));
        assertEquals(dict.get('s'), new Integer(1));
        assertEquals(dict.get(' '), new Integer(2));
    }

    @Test
    public void testCountSpecialCharStr() throws CareerBuilderException {
        Map<Character, Integer> dict = StringHelper.countChars(specialCharStr);
        assertTrue(dict.containsKey('#'));
        assertEquals(dict.get('a'), new Integer(1));
        assertEquals(dict.get('@'), new Integer(1));
        assertEquals(dict.get('#'), new Integer(2));
    }

    @Test
    public void testCountEscapeCharStr() throws CareerBuilderException {
        Map<Character, Integer> dict = StringHelper.countChars(escapeCharStr);
        assertTrue(dict.containsKey('\\'));
        assertEquals(dict.get('|'), new Integer(1));
        assertEquals(dict.get('\\'), new Integer(1));
        assertEquals(dict.get('\n'), new Integer(1));
        assertEquals(dict.get('a'), new Integer(1));
    }

    @Test(expected = CareerBuilderException.class)
    public void testCountNullStr() throws Exception {
        PowerMockito.mockStatic(StringHelper.class);
        PowerMockito.doThrow(new CareerBuilderException(ErrorEnum.NullValueString, StringHelper.class.getName()))
                .when(StringHelper.class, "countChars", Matchers.eq(nullStr));
        StringHelper.countChars(nullStr);
    }

    @Test
    public void testCountEmptyStr() throws CareerBuilderException {
        Map<Character, Integer> dict = StringHelper.countChars(emptyStr);
        assertTrue(dict.isEmpty());
    }

    @Test
    public void testCountCharsAllSpaceStr() throws CareerBuilderException {
        Map<Character, Integer> dict = StringHelper.countChars(allSpaceStr);
        assertTrue(dict.containsKey(' '));
        assertEquals(dict.size(), 1);
        assertEquals(dict.get(' '), new Integer(1));
    }
}
